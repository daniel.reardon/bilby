|pipeline status| |coverage report| |pypi| |conda| |version|

Bilby
=====

A user-friendly Bayesian inference library.
Fulfilling all your Bayesian dreams.

-  `Installation
   instructions <https://lscsoft.docs.ligo.org/bilby/installation.html>`__
-  `Contributing <https://git.ligo.org/lscsoft/bilby/blob/master/CONTRIBUTING.md>`__
-  `Documentation <https://lscsoft.docs.ligo.org/bilby/index.html>`__
-  `Issue tracker <https://git.ligo.org/lscsoft/bilby/issues>`__
-  `Slack workspace <https://bilby-code.slack.com/>`__

We encourage you to contribute to the development via a merge request.  For
help in creating a merge request, see `this page
<https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html>`__ or contact
us directly.

.. |pipeline status| image:: https://git.ligo.org/lscsoft/bilby/badges/master/pipeline.svg
   :target: https://git.ligo.org/lscsoft/bilby/commits/master
.. |coverage report| image:: https://lscsoft.docs.ligo.org/bilby/coverage_badge.svg
   :target: https://lscsoft.docs.ligo.org/bilby/htmlcov/
.. |pypi| image:: https://badge.fury.io/py/bilby.svg
   :target: https://pypi.org/project/bilby/
.. |conda| image:: https://img.shields.io/conda/vn/conda-forge/bilby.svg
   :target: https://anaconda.org/conda-forge/bilby
.. |version| image:: https://img.shields.io/pypi/pyversions/bilby.svg
   :target: https://pypi.org/project/bilby/
